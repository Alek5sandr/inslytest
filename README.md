For running tests:
1. Install Node.JS with NPM.
2. Download the project.
3. Launch 2 CMD/Terminal windows:
  3.1. In one window navigate to the project's folder and enter the command "npm start".
  3.2. In another window navigate to the project's folder and enter the command "npm test".