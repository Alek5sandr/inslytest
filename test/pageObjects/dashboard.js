import Shared from '@/pageObjects/components/shared.js';

class Dashboard extends Shared {
    constructor() {
        super();

        this.locators = {
            userInfo: '#user-info',
            strongTag: 'strong',
            smallTag: 'small'
        };
    }

    get userNameElement() {
        return $(`${this.locators.userInfo}>${this.locators.strongTag}`);
    }

    get loggedInUser() {
        return this.userNameElement.getText();
    }
}

module.exports = new Dashboard();