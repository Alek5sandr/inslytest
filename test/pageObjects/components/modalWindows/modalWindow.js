import Shared from '@/pageObjects/components/shared.js';

class ModalWindow extends Shared {
    constructor() {
        super();

        this.locators = {
            alertWindow: 'div[class^="ui-dialog"]',
            alertWindowTitle: '.ui-dialog-title', // 'Failed: invalid selector' if such CSS selector is used with text search - 'span[class="ui-dialog-title"]'
            xClose: 'span[class="icon-close"]'
        };
    }

    get alertWindowElement() {
        return $(this.locators.alertWindow);
    }

    get alertWindowElements() {
        return $$(this.locators.alertWindow);
    }

    get alertWindowTitleElement() {
        return $(this.locators.alertWindowTitle);
    }

    get xCloseElement() {
        return $(this.locators.xClose);
    }

    isModalWindowOpen(title) {
        return title == null ? super.isElementVisible(this.alertWindowElement) : super.isElementVisible(this.alertWindowElement.$(`${this.locators.alertWindowTitle}=${title}`));
    }

    isModalWindowClosed(title) {
        return title == null ? !(super.isElementVisible(this.alertWindowElement, true)) : !(super.isElementVisible(this.alertWindowElement.$(`${this.locators.alertWindowTitle}=${title}`), true));
    }

    readPassword() {
        return this.alertWindowElement.$(super.sharedLocators.bold).getText();
    }

    closeModalWindow(title) {
        return title == null ? super.clickOnElement(this.xCloseElement) : super.clickOnElement(super.returnRootElement($$(this.locators.alertWindow), title).$(this.locators.xClose));
    }

    //TODO buggy - scrolls main frame down
    scrollModalWindowDownSomePixels(title, pixels) {
        super.returnRootElement(this.alertWindowElements, title).scroll(0, pixels);
        //browser.execute("document.querySelector('div[class^=\"ui-dialog\"] p').scrollTop += 1000"); DOES NOT WORK TOO
    }

    leftMouseClickInModalWindow(title, xoffsetInPercentage, yoffsetInPercentage, counter = 1) {
        const element = super.returnRootElement(this.alertWindowElements, title);
        const width = xoffsetInPercentage * element.getElementSize('width');
        const height = yoffsetInPercentage * element.getElementSize('height');
        for (let i = 0; i < counter; i ++) {
            element.leftClick(Math.round(width), Math.round(height));
        }
    }

}

module.exports = new ModalWindow();
module.exports.classDefinition = ModalWindow;