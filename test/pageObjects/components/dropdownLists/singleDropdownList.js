import Shared from '@/pageObjects/components/shared.js';

class SingleDropdownList extends Shared {
    constructor() {
        super();

        this.locators = {
            selection: 'select',
            option: 'option'
        };
    }

    get selectionElement() {
        return $(this.locators.selection);
    }

    clickOnDropdownList(parentElement, label, defaultOption) {
        const selectionElement = super.returnRootElement(parentElement, label).$(`${this.locators.selection}*=${defaultOption}`);
        selectionElement.click();
        return selectionElement;
    }

    selectOptionFromDropdownList(parentElement, label, defaultOption, option) {
        this.clickOnDropdownList(parentElement, label, defaultOption).click(`${this.locators.option}=${option}`);
    }

}

module.exports = new SingleDropdownList();