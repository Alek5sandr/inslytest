import Shared from '@/pageObjects/components/shared.js';

class InstanceCreation extends Shared {
    constructor() {
        super();

        this.locators = {
            clockIcon: 'div[class="clock"]'
        };
    }

    get clockIconElement() {
        return $(this.locators.clockIcon);
    }

    waitForClockIconToDisappear(timeMS) {
        return super.waitForElementToDisappear(this.clockIconElement, timeMS);
    }
}

module.exports = new InstanceCreation();